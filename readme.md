# Datasheet for twm-restore

## Table of Contents

1. [Motivation](#motivation)
2. [Composition](#composition)
3. [Collection Process](#collection-process)
4. [Preprocessing/Cleaning/Labeling](#preprocessingcleaninglabeling)
5. [Uses](#uses)
6. [Distribution](#distribution)
7. [Maintenance](#maintenance)

## 1. Motivation <a name="motivation"></a>

1. **For what purpose was the dataset created?** 
bki-restore.mbz was created to be importable into Moodle instances and serve as example training and evaluation data for Learning Analytics models like the dropout prediction model. More specifically, it can be used to try out the Moodle plugin [LaLA](https://github.com/LiFaytheGoblin/moodle-tool_lala) which aims to make Moodle Learning Analytics auditable.

2. **Who created the dataset and on behalf of which entity?**
bki-restore.mbz was created by Linda Fernsel in the [Informatik und Gesellschaft](https://iug.htw-berlin.de/) research group at the University of Applied Sciences (HTW) in Berlin. It was reconstructed from the CSV files in the folder "original", which were exported through queries from the Moodle database of the HTW Welcome Moodle.

3. **Who funded the creation of the dataset?** 
The creation of this data set was funded by the Federal Ministry of Education and Research of Germany as part of the project Fair Enough? (project nr: 16DHB4002) at the University of Applied Sciences (HTW) Berlin.

4. **Any other comments?**
No.

## 2. Composition <a name="composition"></a>

1. **What do the instances that comprise the dataset represent?**
The dataset has the form of a Moodle backup file (.mbz) which contains XML files on
* The course structure, modules and properties
* Users enrolled in the course including their
  * activity log
  * grades
  * profile information such as timezone

2. **How many instances are there in total?**
* 1 course
* 12 sections
* 83 course modules
* 14 users, including 1 admin

3. **Does the dataset contain all possible instances or is it a sample (not necessarily random) of instances from a larger set?** 
The original dataset only contained data from those users who consented to the data collection and use in the Brückenkurs Informatik at HTW Berlin in fall 2022.

4. **What data does each instance consist of?** 
The mbz-folder consists of further folders and XML-files. The folder structure is as follows:
```bash
├── activities
│   ├── chat_84
│   ├── choice_95
│   ├── feedback_87
│   ├── feedback_88
│   ├── feedback_89
│   ├── feedback_91
│   ├── feedback_92
│   ├── feedback_93
│   ├── feedback_94
│   ├── folder_101
│   ├── folder_107
│   ├── folder_111
│   ├── folder_115
│   ├── folder_126
│   ├── folder_136
│   ├── forum_166
│   ├── forum_86
│   ├── quiz_100
│   ├── quiz_106
│   ├── quiz_113
│   ├── quiz_127
│   ├── quiz_133
│   ├── quiz_137
│   ├── quiz_141
│   ├── quiz_147
│   ├── quiz_153
│   ├── quiz_96
│   ├── resource_102
│   ├── resource_103
│   ├── resource_104
│   ├── resource_105
│   ├── resource_108
│   ├── resource_109
│   ├── resource_110
│   ├── resource_112
│   ├── resource_116
│   ├── resource_117
│   ├── resource_118
│   ├── resource_119
│   ├── resource_120
│   ├── resource_123
│   ├── resource_124
│   ├── resource_125
│   ├── resource_128
│   ├── resource_129
│   ├── resource_131
│   ├── resource_132
│   ├── resource_134
│   ├── resource_135
│   ├── resource_138
│   ├── resource_140
│   ├── resource_142
│   ├── resource_143
│   ├── resource_145
│   ├── resource_146
│   ├── resource_148
│   ├── resource_149
│   ├── resource_151
│   ├── resource_152
│   ├── resource_155
│   ├── resource_156
│   ├── resource_99
│   ├── url_114
│   ├── url_121
│   ├── url_122
│   ├── url_130
│   ├── url_139
│   ├── url_144
│   ├── url_150
│   ├── url_154
│   ├── url_157
│   ├── url_158
│   ├── url_159
│   ├── url_160
│   ├── url_161
│   ├── url_162
│   ├── url_163
│   ├── url_164
│   ├── url_165
│   ├── url_85
│   ├── url_90
│   ├── url_97
│   └── url_98
├── course
├── files
│   └── a9
└── sections
    ├── section_18
    ├── section_19
    ├── section_20
    ├── section_21
    ├── section_22
    ├── section_23
    ├── section_24
    ├── section_25
    ├── section_26
    ├── section_27
    ├── section_28
    └── section_29
```

An activity may have the following XML files, e.g.:

```bash
│   ├── resource_155
│   │   ├── calendar.xml
│   │   ├── comments.xml
│   │   ├── competencies.xml
│   │   ├── completion.xml
│   │   ├── filters.xml
│   │   ├── grade_history.xml
│   │   ├── grades.xml
│   │   ├── inforef.xml
│   │   ├── logstores.xml
│   │   ├── logs.xml
│   │   ├── module.xml
│   │   ├── resource.xml
│   │   ├── roles.xml
│   │   └── xapistate.xml
```

or

```bash
│   ├── forum_166
│   │   ├── calendar.xml
│   │   ├── comments.xml
│   │   ├── competencies.xml
│   │   ├── completion.xml
│   │   ├── filters.xml
│   │   ├── forum.xml
│   │   ├── grade_history.xml
│   │   ├── grades.xml
│   │   ├── grading.xml
│   │   ├── inforef.xml
│   │   ├── logstores.xml
│   │   ├── logs.xml
│   │   ├── module.xml
│   │   ├── roles.xml
│   │   └── xapistate.xml
```

Activity logs are stored in the "logstores.xml" files in the folder concerned by the respective logs.

5. **Is there a label or target associated with each instance?**
No.

6. **Is any information missing from individual instances?** 
No personal data is contained in the data set. All content was purged from the "other" column in the logstore table. BigBlueButton activity logs were excluded. The CSV files did not allow to exactly reconstruct the course structure. There are possibly course modules missing and the order might be wrong. Furthermore, the course modules are "dummy" elements without any real content. 

7. **Are relationships between individual instances made explicit?**
The structure of the folder reveals some relationships - the logs and grades belonging to a course module are stored in the respective folder.

8. **Are there recommended data splits?** 
No.

9. **Are there any errors, sources of noise, or redundancies in the dataset?** 
The reconstruction of the course tried to approximate the original course structure and content but should not be seen as a perfect reconstruction. As written before, a lot of data that would really be exported from a Moodle course is not contained. 

10. **Is the dataset self-contained, or does it link to or otherwise rely on external resources?** 
The data set is in a form that is most useful when imported into a Moodle system. The import has been tested with Moodle 4.2.

11. **Does the dataset contain data that might be considered confidential?** 
No.

12. **Does the dataset contain data that, if viewed directly, might be offensive, insulting, threatening, or might otherwise cause anxiety?** 
No.

13. **Does the dataset identify any subpopulations?**
No.

14. **Is it possible to identify individuals (i.e., one or more natural persons), either directly or indirectly (i.e., in combination with other data) from the dataset?** 
No.

15. **Does the dataset contain data that might be considered sensitive in any way?** 
No.

16. **Any other comments?**
No.

## 3. Collection Process <a name="collection-process"></a>


1. **How was the data associated with each instance acquired?** Was the data directly observable (e.g., raw text, movie ratings), reported by subjects (e.g., survey responses), or indirectly inferred/derived from other data (e.g., part-of-speech tags, model-based guesses for age or language)? If the data was reported by subjects or indirectly inferred/derived from other data, was the data validated/verified? If so, please describe how.
The data was directly created by the plugin Level Up Log on the Moodle platform, from the students’ clicks.
The CSV data was queried from the corresponding HTW Welcome Moodle database tables. Only the course modules as well as those logs and grades were queried, that belong to users who consented to the data collection. The queries were as follows (with the `{}` indicating a value to be replaced in the actual query):

```postgresql
SELECT course, module, instance, section, added, name 
FROM mdl_course_modules 
LEFT JOIN mdl_modules
ON mdl_course_modules.module=mdl_modules.id
WHERE course={};
```

```postgresql
SELECT itemid, userid, rawgrade, finalgrade, timemodified 
FROM mdl_grade_grades
WHERE userid IN {}
AND itemid IN (SELECT id AS itemid 
		FROM mdl_grade_items 
		WHERE courseid={});
```

```postgresql
SELECT userid, id, eventname, component, action, target, objecttable, objectid, crud, edulevel, contextid, contextlevel, contextinstanceid, courseid, timecreated, relateduserid
FROM mdl_logstore_standard_log
WHERE courseid={}
AND userid IN {}
AND (objecttable IS NULL 
OR ((objecttable <> 'bigbluebuttonbn') 
AND (objecttable <> 'feedback_completed')));
```

2. **What mechanisms or procedures were used to collect the data?**
SQL-Queries. The resulting data has been scanned manually for plausibility.

3. **If the dataset is a sample from a larger set, what was the sampling strategy?**
Self-selection.

4. **Who was involved in the data collection process and how were they compensated?**
Students consented to provide their data or rejected it. The HTW Moodle administrator executed the queries.
No one received compensation.

5. **Over what timeframe was the data collected?** 
The data was produced between August 30, 2022, until October 21, 2022.


6. **Were any ethical review processes conducted?**
The data collection was discussed with the course teacher, who discussed with the students about general requirements for the data collection. This process revealed that the students did not want to share the BigBlueButton logs, which we respected when querying the data.

7. **Did you collect the data from the individuals in question directly, or obtain it via third parties or other sources?**
The data in the CSV tables was automatically logged by Moodle, then queried from the data base and transformed into an mbz.


8. **Were the individuals in question notified about the data collection?**
Yes. They received the following information along the possibility to agree or disagree:

```
Sehr geehrte:r Studierende:r,

Bei einem Projekt der Forschungsgruppe "Informatik und Gesellschaft" an der HTW Berlin sollen Features von Lernplattformen wie Moodle getestet werden um zu überprüfen, ob diese Features gleich gut für unterschiedliche Lerntypen funktionieren. Dafür werden realistische Testdaten benötigt. Aus diesem Grund sind Sie eingeladen, ihre Daten der Forschungsgruppe zur Verfügung zu stellen.

Ihre Daten werden dabei anonymisiert. Das bedeutet, Sie erhalten eine ID, die von der Forschungsgruppe nicht mehr auf Sie zurückgeführt werden kann. Eventueller Text, der in den Logdaten vorhanden ist, oder IP-Adressen, von dem/denen man auf Sie schließen könnte, werden nicht erhoben. 

Welche Daten werden gesammelt?
Von Ihnen werden, falls Sie dem zustimmen, folgende anonymisierte Daten erhoben:
(1) Ihre Logdaten im Informatik-Brückenkurs, also was Sie im Kurs tun, worauf Sie klicken, usw., mit Ausnahme der Logs die BigBlueButton betreffen.
(2) Ihre Bewertungen im Informatik-Brückenkurs, also Noten oder Punkte die Sie für Aufgaben bekommen.

Wie werden die Daten gespeichert, verarbeitet und verwendet?
Ihre Daten werden, wenn Sie zustimmen, zunächst HTW-intern für Forschungszwecke gespeichert, verarbeitet und verwendet.
Ihre anonymisierten Daten können, wenn wir eine Rückidentifizierbarkeit ausschließen können, in Zukunft veröffentlicht werden, und dabei von allen nach Belieben gespeichert, verarbeitet, verändert, verwendet und weitergegeben werden. 

Was passiert wenn ich meine Daten nicht zur Verfügung stellen will?
Ihre Einwilligung ist selbstverständlich optional. Sollten Sie nicht einwilligen, hat dies keinerlei Nachteile für Sie. 

Was, wenn ich mich umentscheide?
Sollten Sie sich vor dem Export der anonymisierten Daten umentscheiden und Ihre Einwilligung zurückziehen wollen, aktualisieren Sie bitte Ihre Antwort in diesem Formular. Nach dem Export ist das nachträgliche Entfernen Ihrer Daten nicht mehr möglich, da nicht mehr identifiziert werden kann, welche Daten zu Ihnen gehören.


Kontakt:
Datenexporteur/Moodle-Administrator: <a href="https://www.htw-berlin.de/hochschule/personen/person/?eid=8720">Sebastian Homer</a>

Projektleitung: <a href="https://www.htw-berlin.de/hochschule/personen/person/?eid=12447">Linda Fernsel</a>

Leitung der Forschungsgruppe: <a href="https://www.htw-berlin.de/hochschule/personen/person/?eid=9862">Prof. Dr. Simbeck</a>


Einwilligung:

Mit "Ja" bestätige ich, dass ich meine anonymisierten Daten wie oben beschrieben zur Verfügung stelle.
```

9. **Did the individuals in question consent to the collection and use of their data?** 
See above.


10. **If consent was obtained, were the consenting individuals provided with a mechanism to revoke their consent in the future or for certain uses?**
See above.

11. **Has an analysis of the potential impact of the dataset and its use on data subjects been conducted?** 
No.

12. **Any other comments?**
No.

## 4. Preprocessing/Cleaning/Labeling <a name="preprocessingcleaninglabeling"></a>

1. **Was any preprocessing/cleaning/labeling of the data done?** 
The CSV files were transformed into an mbz. During that process:
* The grade history was reduced to only the most recent grade per student per activity and transformed into a CSV file that can be imported to the Moodle "gradebook". It was used to reconstruct the grades in the mbz file.
* New ids were assigned to entities.

2. **Was the "raw" data saved in addition to the preprocessed/cleaned/labeled data?**
Yes, see the folder "original".

3. **Is the software that was used to preprocess/clean/label the data available?** 
Yes, the data transformation [CSV to MBZ pipeline](https://gitlab.com/iug-research/moodle-learning-analytics/csv-to-mbz-pipeline/-/tree/bki?ref_type=heads) is available (branch `bki`).

4. **Any other comments?**
No.

## 5. Uses <a name="uses"></a>

1. **Has the dataset been used for any tasks already?**
The dataset serves as a test data set for the LaLA plugin.

2. **Is there a repository that links to any or all papers or systems that use the dataset?** 
No.

3. **What (other) tasks could the dataset be used for?**
Further test Moodle Learning Analytics - however, the data set is too small for training a model and would need to be complemented with further mbz files.

4. **Is there anything about the composition of the dataset or the way it was collected and preprocessed/cleaned/labeled that might impact future uses?** 
Different versions of Moodle might not be able to restore the mbz in the same ways.

5. **Are there tasks for which the dataset should not be used?**
No.

6. **Any other comments?**
No.

## 6. Distribution <a name="distribution"></a>

1. **Will the dataset be distributed to third parties outside of the entity on behalf of which the dataset was created?** 
Yes, the dataset is published via this repository.

2. **How will the dataset be distributed?**
GitLab.

3. **When will the dataset be distributed?**
December 2023

4. **Will the dataset be distributed under a copyright or other intellectual property (IP) license, and/or under applicable terms of use (ToU)?** 
[CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)

5. **Have any third parties imposed IP-based or other restrictions on the data associated with the instances?**
No.

6. **Do any export controls or other regulatory restrictions apply to the dataset or to individual instances?**
No.

7. **Any other comments?**
No.

## 7. Maintenance <a name="maintenance"></a>

1. **Who will be supporting/hosting/maintaining the dataset?**
The [Informatik und Gesellschaft](https://iug.htw-berlin.de/) research group at HTW Berlin.

2. **How can the owner/curator/manager of the dataset be contacted?**
See [the website](https://iug.htw-berlin.de/)

3. **Is there an erratum?**
No.

4. **Will the dataset be updated?** 
No.

5. **If the dataset relates to people, are there applicable limits on the retention of the data associated with the instances?** 
People consented to the use of their data for research purposes only.

6. **Will older versions of the dataset continue to be supported/hosted/maintained?** 
Older versions (if any) will be available via Git versioning.

7. **If others want to extend/augment/build on/contribute to the dataset, is there a mechanism for them to do so?**
No.

8. **Any other comments?**
No.
   
# About Datasheets for Datasets
Datasheets for Datasets is a documentation method for data that was introduced in the following publication:

Timnit Gebru, Jamie Morgenstern, Briana Vecchione, Jennifer Wortman Vaughan, Hanna Wallach, Hal Daumé III, and Kate Crawford. 2021. Datasheets for datasets. Commun. ACM 64, 12 (December 2021), 86–92. https://doi.org/10.1145/3458723
